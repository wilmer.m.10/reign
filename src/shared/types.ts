/** types for data that comes from the server */

export type PostType = {
	story_title: string;
	story_url: string;
	created_at: string;
	author: string;
	objectID: string;
};
