import React from 'react';
import './button-reload.css';


type ButtonReloadProps = {
	onReload: () => void;
}

/**
 * @param props - Props received.
 * @param {Function} props.onReload - reload function.
 * 
 * @returns {JSX.Element} - Button reload.
 */
const ButtonReload = ({
	onReload
}: ButtonReloadProps): JSX.Element => {
	return (
		<div className='button-reload-container'>
			<div>
				<p className='button-reload-text' >Error occurred</p>
				<button className='button-reload' onClick={onReload}>Refresh </button>
			</div>
		</div>
	)
}

export default ButtonReload;