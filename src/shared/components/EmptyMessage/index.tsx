import React from 'react';
import './empty-message.css';

/**
 * @returns {JSX.Element} - empty message.
 */
const EmptyMessage = (): JSX.Element => {
	return (
		<div className='empty-message'>
			<div>No records found</div>
		</div>
	)
};

export default EmptyMessage;