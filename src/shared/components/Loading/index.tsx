import React from 'react';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import './loading.css';

type LoadingProps = {
	height: number;
	width: number;
}

/**
 * 
 * @returns {JSX.Element} - Loading container.
 */
export const Loading = ({
	height,
	width
}: LoadingProps): JSX.Element => (
	<div className='loading-container'>
		<div className='loading-item'>
			<Loader type="Circles" color="#00BFFF" height={height} width={width} />
		</div>
	</div>
);

Loading.defaultProps = {
	height: 80,
	width: 80
};
