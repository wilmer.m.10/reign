import React from 'react';
import './container.css'

type ContainerProps = {
	children: JSX.Element;
}

/**
 * @param props - Props received.
 * @param {JSX.Element} props.children - Children components.
 * 
 * @returns {JSX.Element} - Container wrapper.
 */
const Container = ({
	children
}: ContainerProps): JSX.Element => {
	return (
		<div className='container'>{children}</div>
	)
}

export default Container;
