import React from 'react';
import Container from '../Container';
import './header.css';

/**
 * 
 * @returns {JSX.Element} - Header component.
 */
const Header = (): JSX.Element => (
	<div className='header-container'>
		<Container>
			<p className='header-text'>
				HACKER NEWS
			</p>
			
		</Container>
	</div>
);

export default Header;