import React from 'react';
import Container from '../Container';
import Header from '../Header';

type LayoutProps = {
	children: JSX.Element;
}


/**
 * 
 * @param props - Props received.
 * @param {JSX.Element} props.children - Children component
 * 
 * @returns {JSX.Element} - Layout wrapper. 
 */
const Layout = ({ 
	children 
}: LayoutProps): JSX.Element => {
	return (
		<>
			<Header />
			<Container>
				{children}
			</Container>
		</>
	)
}


export default Layout;
