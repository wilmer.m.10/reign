import React, { Dispatch, SetStateAction } from 'react';
import ReactPaginate from 'react-paginate';
import './pagination.css';

type PaginationType = {
	setPaginationData: Dispatch<SetStateAction<{
		page: number;
		framework: string;
	}>>;
	paginationData: {
		page: number;
		framework: string;
	},
	totalPages: number;
}

type PageParamType = {
	selected: number;
}

/**
 * @param props - Props received
 * @param props.paginationData - Pagination data.
 * @param {number} props.totalPages - Total pages.
 * @param {Dispatch<SetStateAction>} props.setPaginationData - Function to set pagination data.
 * 
 * @returns {JSX.Element} - Pagination componente
 */
export const Paginate = ({
	paginationData,
	totalPages,
	setPaginationData
}: PaginationType) => {
	let content: JSX.Element | null = null;

	const handleChangePage = (selectedItem: PageParamType): void => {
		setPaginationData({
			...paginationData,
			page: selectedItem.selected
		});
	};

	if (totalPages > 0) {
		content = (
			<ReactPaginate
				containerClassName="pagination-container"
				pageClassName="pagination-item"
				pageLinkClassName="pagination-item-text"
				nextClassName="pagination-item"
				nextLinkClassName="pagination-item-text"
				previousClassName="pagination-item"
				previousLinkClassName="pagination-item-text"
				breakClassName="pagination-item-none"
				activeClassName="pagination-item-active"
				activeLinkClassName="pagination-item-text-active"
				nextLabel=">"
				forcePage={paginationData.page}
				onPageChange={handleChangePage}
				pageRangeDisplayed={10}
				pageCount={totalPages}
				previousLabel="<"
				marginPagesDisplayed={0}
			/>
		)
	}
	
	return (
		content
	);
}