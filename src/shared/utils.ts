/**
 * @param {value} - Number to check.
 * 
 * @returns {boolean} - if is number par.
 */
export const isPar = (value: number): boolean => (
	(value % 2) === 0
);