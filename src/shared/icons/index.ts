import { ReactComponent as FavoriteOutline } from './iconmonstr-favorite-2.svg';
import { ReactComponent as Favorite } from './iconmonstr-favorite-3.svg';
import { ReactComponent as Time } from './iconmonstr-time-2.svg';
import { ReactComponent as  Chevron } from './chevron-down.svg';


export const FavoriteOutlineIcon = FavoriteOutline;
export const FavoriteIcon = Favorite;
export const TimeIcon = Time;
export const ChevronIcon = Chevron;