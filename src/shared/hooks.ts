import { useEffect } from 'react';


export const useOutsideClick = (ref: React.MutableRefObject<HTMLDivElement | null>, cb: () => void) => {
	useEffect(() => {
		const handleOutside = (evt: MouseEvent) => {
			if (ref.current && !ref.current.contains(evt.target as Node)) {
				cb();
			}
		};

		document.addEventListener('mousedown', handleOutside);

		return () => {
			document.removeEventListener('mousedown', handleOutside);
		};
		
	}, [ref, cb]);
}