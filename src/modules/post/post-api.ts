import { CLIENT_URL } from '../../shared/constants';

export const fetchPostsApi = async (framework: string, page: number) => {
	const url = `${CLIENT_URL}/search_by_date?query=${framework}&page=${page}`;
	const response = await fetch(url);
	const data = await response.json();

	return data;
}