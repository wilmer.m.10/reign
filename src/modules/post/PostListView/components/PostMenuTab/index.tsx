import React, { Dispatch, SetStateAction } from 'react';
import './post-menu-tab.css';

type PostMenuTabProps = {
	showFavorites: boolean;
	setShowFavorites: Dispatch<SetStateAction<boolean>>;
}

/**
 * @param {boolean} props.showFavorites - State to show favorites.
 * @param {Dispatch<SetStateAction<boolean>>} props.setShowFavorites - Function to change show status.
 * 
 * @returns {JSX.Element} - Menu tab component.
 */
const PostMenuTab = ({
	showFavorites,
	setShowFavorites
}: PostMenuTabProps): JSX.Element => {
	let allClass = 'post-tab-item-selected post-tab-selected-all';
	let favClass = '';

	if (showFavorites) {
		allClass = '';
		favClass = 'post-tab-item-selected post-tab-selected-faves';
	}
	
	return (
		<div className='post-tab'>
			<div 
				className={`post-tab-item ${allClass}`}
				onClick={() => setShowFavorites(false)}
			>
				All
			</div>
			<div className={`post-tab-item ${favClass}`}
				onClick={() => setShowFavorites(true)}
			>
				My Faves
			</div>
		</div>
	)	
}

export default PostMenuTab;