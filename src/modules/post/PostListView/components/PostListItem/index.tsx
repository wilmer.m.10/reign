import React, { useContext } from 'react';
import { FavoriteOutlineIcon, FavoriteIcon, TimeIcon } from '../../../../../shared/icons';
import { PostType } from '../../../../../shared/types';
import { PostListContext } from '../PostListProvider';
import moment from 'moment';
import './post-list-item.css';

type PostProps = {
	post: PostType;
};


/**
 * 
 * @param props - Props received.
 * @param {PostType} props.post - Post data.
 * 
 * @returns {JSX.Element} - Post item component.
 */
const PostListItem = ({
	post
}: PostProps): JSX.Element => {
	const { favorites, setFavorites } = useContext(PostListContext);
	
	const handleGoToUrl = (): void => {
		location.assign(post.story_url);
	};
	
	const handleAddFavorite = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
		e.stopPropagation();
		const selectedFavorites = [...favorites, post];

		localStorage.setItem('favorites', JSON.stringify(selectedFavorites));
		setFavorites(selectedFavorites);
	};

	const handleRemoveFavorite = (e: React.MouseEvent<HTMLDivElement, MouseEvent>): void => {
		e.stopPropagation();
		
		const selectedFavorites = favorites.filter((favorite) => (
			favorite.objectID !== post.objectID
		));

		localStorage.setItem('favorites', JSON.stringify(selectedFavorites));
		setFavorites(selectedFavorites);
	};

	const isFavorite = (): boolean => {
		return favorites.some((favorite) => (
			favorite.objectID === post.objectID
		));
	};

	let favoriteIcon = (
		<div className='post-favorite-container' onClick={handleAddFavorite}>
			<FavoriteOutlineIcon />
			<div className='post-favorite-overlay' />
		</div>
	);

	if (isFavorite()) {
		favoriteIcon = (
			<div className='post-favorite-container' onClick={handleRemoveFavorite}>
				<FavoriteIcon />
				<div className='post-favorite-overlay' />
			</div>
		);
	}

	return (
		<div className='post-item' onClick={handleGoToUrl}>
			<div className='post-info-container'>
				<div className='post-time'>
					<div className='post-time-icon-container'>
						<TimeIcon />
					</div>
					<p className='post-time-text'>
						{moment(post.created_at).fromNow()} by {post.author}
					</p>
				</div>
				<div className='post-content'>
					<p className='post-content-text'>
						{post.story_title}
					</p>
				</div>
			</div>
			{favoriteIcon}
			<div className='post-item-overlay' />
		</div>
	);
}

export default PostListItem;