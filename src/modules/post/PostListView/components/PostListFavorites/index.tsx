import React, { useContext }  from 'react';
import PostListItem from '../PostListItem';
import { isPar } from '../../../../../shared/utils';
import { PostListContext } from '../PostListProvider';
import EmptyMessage from '../../../../../shared/components/EmptyMessage';
import './post-list-favorites.css';



/**
 * 
 * @returns {JSX.Element} - Favorite post.
 */
const PostListFavorites = (): JSX.Element => {
	const { favorites } = useContext(PostListContext);

	let content: JSX.Element | JSX.Element[] = <EmptyMessage />;


	if (favorites.length) {
		content = favorites.map((post, idx) => {
			const rightClass = isPar(idx) ? 'post-favorites-item-right' : '';
			const key = `${post.story_title}-${post.created_at}-${post.objectID}`;

			return (
				<div className={`post-favorites-item ${rightClass}`} key={key}>
					<PostListItem post={post} />
				</div>
			)
		});
	}

	
	
	return (
		<div className='post-favorites-content'>
			{content}
		</div>
	);
};

export default PostListFavorites;
