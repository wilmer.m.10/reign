import React, { createContext, Dispatch, SetStateAction } from 'react';
import { PostType } from '../../../../../shared/types';

type PostListContextProps = {
	setPaginationData: Dispatch<SetStateAction<{
		page: number;
		framework: string;
	}>>;
	setFavorites: Dispatch<SetStateAction<PostType[]>>;
	paginationData: {
		page: number;
		framework: string;
	},
	favorites: PostType [];
	totalPages: number;
	loading: boolean;
}

type PostListCtxProviderProps = {
	setPaginationData: Dispatch<SetStateAction<{
		page: number;
		framework: string;
	}>>;
	setFavorites: Dispatch<SetStateAction<PostType[]>>;
	paginationData: {
		page: number;
		framework: string;
	},
	favorites: PostType[];
	totalPages: number;
	loading: boolean;
	children: JSX.Element;
}


export const PostListContext = createContext<PostListContextProps>({
	paginationData: {
		page: 0,
		framework: 'angular'
	},
	setFavorites: () => ([]),
	setPaginationData: () => ({
		page: 0,
		framework: 'angular'
	}),
	favorites: [],
	totalPages: 0,
	loading: false
});

/**
 * @param props - Props received.
 * @param props.paginationData - Pagination data.
 * @param {PostType []} props.favorites - Post favorites data.
 * @param {Dispatch<SetStateAction>} props.setPaginationData - Function to set pagination data.
 * @param {Dispatch<SetStateAction>} props.setFavorites - Function to set favorites post.
 * @param {number} props.totalPages - Total number pages.
 * @param {boolean} props.loading - Loding state.
 * @param {JSX.Element} props.children - children component.
 * @returns {JSX.Element} Post list provider.
 */
export const PostListProvider = ({
	paginationData,
	totalPages,
	loading,
	setPaginationData,
	setFavorites,
	favorites,
	children
}: PostListCtxProviderProps): JSX.Element => {
	return (
		<PostListContext.Provider
			value={{
				paginationData,
				totalPages,
				loading,
				favorites,
				setFavorites,
				setPaginationData
			}}
		>
			{children}
		</PostListContext.Provider>
	)
}