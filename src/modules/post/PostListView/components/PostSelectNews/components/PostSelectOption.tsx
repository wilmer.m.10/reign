import React from 'react';
import './post-select-option.css';

export type OptionNewType = {
	value: string;
	name: string;
	logo: string;
};

type PostSelectOptionProps = {
	optionNew: OptionNewType;
	onChangeOption: (optionNew: OptionNewType) => void;
};

/**
 * @param props - Props Received.
 * @param {OptionNewType} optionNew - Option New.
 * @param {Function} onChangeOption - Function to change option.
 * @returns {JSX.Element} Select option.
 */
const PostSelectOption = ({
	optionNew,
	onChangeOption
}: PostSelectOptionProps): JSX.Element => {

	
	return (
		<div className='post-option' onClick={() => onChangeOption(optionNew)}>
			<div className='post-option-img-container'>
				<img src={optionNew.logo} alt="angular logo" className='post-option-img' />
			</div>
			<div>
				<p className='post-option-text'>
					{optionNew.name}
				</p>
			</div>
		</div>
	)
};

export default PostSelectOption;