import React, { useState, useContext, useRef } from 'react';
import { ChevronIcon } from '../../../../../shared/icons';
import PostSelectOption, { OptionNewType } from './components/PostSelectOption';
import AngularLogo from '../../../../../shared/assets/angular-logo/image-138@2x.png';
import ReactLogo from '../../../../../shared/assets/react-logo/image-140@2x.png';
import VueLogo from '../../../../../shared/assets/vue-logo/image-141@2x.png';
import { PostListContext } from '../PostListProvider';
import { useOutsideClick } from '../../../../../shared/hooks';

import './post-select-news.css';

const OPTION_NEWS: OptionNewType [] = [
	{
		value: 'angular',
		name: 'Angular',
		logo: AngularLogo
	},
	{
		value: 'reactjs',
		name: 'React',
		logo: ReactLogo
	},
	{
		value: 'vuejs',
		name: 'Vue',
		logo: VueLogo
	},
]

/**
 * 
 * @returns {JSX.Element} - Select for news.
 */
const PostSelectedNews = (): JSX.Element => {
	const { setPaginationData, paginationData } = useContext(PostListContext);
	const [showMenu, setShowMenu] = useState<boolean>();
	const [selectedOption, setSelectOption] = useState<OptionNewType | null>(() => {
		const storageOption = localStorage.getItem('selectedOption');

		if (!storageOption) return null;

		return JSON.parse(storageOption);
	});

	const outsideRef = useRef<null | HTMLDivElement>(null);
	useOutsideClick(outsideRef, () => setShowMenu(false));
	
	let options: JSX.Element | null = null;
	
	let optionUI: JSX.Element = (
		<p className='post-select-text'>
			Select your news
		</p>
	);

	const handleToggleMenu = (): void => setShowMenu((prevShow) => !prevShow);

	const handleChangeOption = (optionNew: OptionNewType): void => {
		setSelectOption(optionNew);
		setShowMenu(false);
		setPaginationData({
			...paginationData,
			page: 0,
			framework: optionNew.value
		});

		localStorage.setItem('selectedOption', JSON.stringify(optionNew));
	};


	if (selectedOption) {
		optionUI = (
			<div className='post-select-item-container'>
				<div className='post-select-item-img-container'>
					<img
						src={selectedOption.logo}
						alt={`${selectedOption.name} logo`}
						className='post-select-item-img'
					/>
				</div>
				<div>
					<p className='post-select-item-text'>
						{selectedOption.name}
					</p>
				</div>
			</div>
		)
	}

	if (showMenu) {
		const list = OPTION_NEWS.map((optionNew) => (
			<PostSelectOption 
				optionNew={optionNew} 
				key={optionNew.name} 
				onChangeOption={handleChangeOption}
			/>
		));

		options = (
			<div className='post-select-options'>
				{list}
			</div>
		);
	}
	
	return (
		<div className='post-select-container' ref={outsideRef}>
			<div className='post-select' onClick={handleToggleMenu}>
				<div>
					{optionUI}
				</div>
				<div className='post-select-icon'>
					<ChevronIcon />
				</div>
			</div>
			{options}
		</div>
	);
}

export default PostSelectedNews;