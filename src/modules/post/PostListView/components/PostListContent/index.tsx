import React, { useContext } from 'react';
import PostListItem from '../PostListItem';
import { PostType } from '../../../../../shared/types';
import { isPar } from '../../../../../shared/utils';
import { Paginate } from '../../../../../shared/components/Pagination';
import { Loading } from '../../../../../shared/components/Loading';
import { PostListContext } from '../PostListProvider';
import PostSelectedNews from '../PostSelectNews';
import EmptyMessage from '../../../../../shared/components/EmptyMessage';
import ButtonReload from '../../../../../shared/components/ButtonReload';
import './post-list-content.css';


type PostListContentProps = {
	posts: PostType[];
	error: boolean;
	onReload: () => void;
}

/**
 * @param props - Props received.
 * @param {PostType[]} props.posts - Posts data.
 * @param {boolean} props.error - Error status.
 * @param {Function} props.onReload - Function to reload.
 * 
 * @returns {JSX.Element} - Post List. 
 */
const PostListContent = ({
	posts,
	error,
	onReload
}: PostListContentProps): JSX.Element => {
	const { loading, setPaginationData, paginationData, totalPages } = useContext(PostListContext);

	let content: JSX.Element | JSX.Element[] = <EmptyMessage />;

	let paginationUI: JSX.Element | null = (
		<div className = 'post-list-pagination-container' >
			<Paginate
				paginationData={paginationData}
				setPaginationData={setPaginationData}
				totalPages={totalPages}
			/>
		</div >
	);

	
	if (posts.length) {
		content = posts.map((post, idx) => {
			const rightClass = isPar(idx) ? 'post-list-item-right' : '';
			const key = `${post.story_title}-${post.created_at}-${post.objectID}`;

			return (
				<div className={`post-list-item ${rightClass}`} key={key}>
					<PostListItem post={post} />
				</div>
			)
		});
	}

	if (loading) {
		content = (
			<div className='post-list-loading-container'>
				<Loading />
			</div>
		);
	}

	if (error) {
		paginationUI = null;
		content = (
			<ButtonReload onReload={onReload} />
		);
	}
	
	return (
		<>
			<PostSelectedNews />
			<div className='post-list-content'>
				{content}
			</div>
			{paginationUI}
		</>
	);
}

export default PostListContent;