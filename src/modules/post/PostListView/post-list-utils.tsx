import { PostType } from '../../../shared/types';
import { OptionNewType } from './components/PostSelectNews/components/PostSelectOption';
import pick from 'lodash/pick';

export const getFavorites = (): PostType[] => {
	const favoritesStr = localStorage.getItem('favorites');
	let favorites: PostType[] = [];

	if (favoritesStr) {
		favorites = JSON.parse(favoritesStr) as PostType[];
	}

	return favorites;
};


export const getSelectedOption = (): OptionNewType | null => {
	const storageOption = localStorage.getItem('selectedOption');

	if (storageOption) {
		return JSON.parse(storageOption) as OptionNewType;
	}

	return null;
};


export const filterHits = (hitsList: PostType[]): PostType[] => {
	const hits = hitsList.filter((hit: PostType) => {
		const post = pick(hit, ['author', 'story_title', 'story_url', 'created_at']);
		const values = Object.values(post);

		return values.every((val) => val);
	});

	return hits;
};

export const transformHitFormat = (hitsList: PostType[]): PostType[] => {
	const hits = hitsList.map((hit: PostType) => {
		const post = pick(hit, ['author', 'story_title', 'story_url', 'created_at', 'objectID']);

		return post;
	});

	return hits;
};