import React, { useEffect, useCallback, useState } from 'react';
import PostMenuTab from './components/PostMenuTab';
import PostListContent from './components/PostListContent';
import Layout from '../../../shared/components/Layout';
import { fetchPostsApi } from '../post-api';
import { PostType } from '../../../shared/types';
import { PostListProvider } from './components/PostListProvider';
import PostListFavorites from './components/PostListFavorites';

import {
	getFavorites, 
	getSelectedOption,
	filterHits,
	transformHitFormat 
} from './post-list-utils';

/**
 * 
 * @returns {JSX.Element} - Post list view.
 */
const PostListView = (): JSX.Element => {
	const [showFavorites, setShowFavorites] = useState<boolean>(false);
	const [error, setError] = useState<boolean>(false);
	const [totalPages, setTotalPages] = useState<number>(0);
	const [loading ,setLoading] = useState<boolean>(true);
	const [posts, setPosts] = useState<PostType[]>([]);
	const [favorites, setFavorites] = useState<PostType []>(getFavorites());
	const [paginationData, setPaginationData] = useState(() => {
		const storageOption = getSelectedOption();
		const framework = storageOption ? storageOption.value : 'angular';

		return {
			page: 0,
			framework
		};
	});

	const fetchPosts = useCallback(async () => {
		setLoading(true);
		setError(false);

		let response;

		try {
			response = await fetchPostsApi(paginationData.framework, paginationData.page);
		} catch {
			setError(true);
		}

		if (response) {
			let hits = filterHits(response.hits);
			hits = transformHitFormat(hits);
			setPosts(hits);
			setTotalPages(response.nbPages);
		}

		setLoading(false);
	}, [paginationData]);

	useEffect(() => {
		fetchPosts();
	}, [fetchPosts]);

	let list = (
		<PostListContent 
			error={error}
			onReload={fetchPosts}
			posts={posts} 
		/>
	);

	if (showFavorites) {
		list = (
			<PostListFavorites />
		);
	}

	return (
		<Layout>
			<PostListProvider 
				totalPages={totalPages}
				setPaginationData={setPaginationData}
				setFavorites={setFavorites}
				favorites={favorites}
				paginationData={paginationData}
				loading={loading}
			>
				<>
					<PostMenuTab  
						showFavorites={showFavorites}
						setShowFavorites={setShowFavorites}
					/>
					{list}
				</>
			</PostListProvider>
		</Layout>
	)
};

export default PostListView;