import React from 'react';
import PostListView from './modules/post/PostListView';

function App() {
  return (
    <PostListView />
  );
}

export default App;
